@extends('admin.admin_master')
@section('admin')
<div class="page-content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-0">Blog List Data</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Blog Data</h4>
                        <table id="datatable" class="table table-bordered dt-responsive nowrap"
                            style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Blog Category</th>
                                    <th>Blog Title</th>
                                    <th>Blog Tags</th>
                                    <th>Blog Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php($i=1)
                                @foreach($blogs as $item)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <th>{{ $item['category']['blog_category'] }}</th>
                                    <th>{{ $item->blog_title }}</th>
                                    <th>{{ $item->blog_tags }}</th>
                                    <td><img src="{{ asset($item->blog_image) }}" alt="" width="60" height="60"></td>
                                    <td>
                                        <a href="{{ route('edit.blog', $item->id) }}" class="btn btn-info sm"
                                            title="Edit Data"><i class="fas fa-edit"></i></a>
                                        <a href="{{ route('delete.blog', $item->id) }}" class="btn btn-danger sm"
                                            title="Delete Data" id="delete"><i class="fas fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div> <!-- container-fluid -->
</div>
@endsection