<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BlogCategory;
use Illuminate\Support\Carbon;

class BlogCategoryController extends Controller
{
    public function AllBlogCategory() { 
        $blogCategory = BlogCategory::latest()->get();
        return view('admin.blog_category.blog_category_all', compact('blogCategory'));
    }//end method

    public function AddBlogCategory() { 
        return view('admin.blog_category.blog_category_add');
    }//end method

    public function StoreBlogCategory(Request $request){
        $request->validate([
            'blog_category' => 'required'
        ],[
            'blog_category.required' => 'Blog Category Name is required'
        ]);

        BlogCategory::insert([
            'blog_category' => $request->blog_category,
            'created_at' => Carbon::now(),
        ]);

        $notification = array(
            'message' => 'Blog Category Inserted Successfully',
            'alert-type' => 'info'
        );
        return redirect()->route('all.blog.category')->with($notification);
    }//end method

    public function EditBlogCategory($id) {
        $blogCategory = BlogCategory::findOrFail($id);
        return view('admin.blog_category.blog_category_edit', compact('blogCategory'));
    }//end method

    public function UpdateBlogCategory(Request $request,$id){
        BlogCategory::findOrFail($id)->update([
            'blog_category' => $request->blog_category,
        ]);
        $notification = array(
            'message' => 'Blog Category update Successfully',
            'alert-type' => 'info' 
        );
        return redirect()->route('all.blog.category')->with($notification);
    }//end method

    public function DeleteBlogCategory($id) {
        BlogCategory::findOrFail($id)->delete();
        $notification = array(
            'message' => 'Blog Category Deleted Successfully',
            'alert-type' => 'info' 
        );
        return redirect()->back()->with($notification);
    }//end method
}
