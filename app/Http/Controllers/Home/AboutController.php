<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\About;
use App\Models\MultiImage;
use Image;
use Illuminate\Support\Carbon;

class AboutController extends Controller
{
    public function AboutPage() {  
        $aboutPage = About::find(1);
        return view('admin.about_page.about_page_all', compact('aboutPage'));
    } // end 

    public function UpdateAbout(Request $request){
        $about_id = $request->id;
        if($request->file('about_image')) {
            $image = $request->file('about_image'); 
            $name_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension(); //id.jpg(dinh dang ten anh)
            Image::make($image)->resize(523,605)->save('upload/home_about/'.$name_gen);
            $save_url = 'upload/home_about/'.$name_gen; //save db

            About::findOrFail($about_id)->update([
                'title' => $request->title,
                'short_title' => $request->short_title,
                'short_description' => $request->short_description,
                'long_description' => $request->long_description,
                'about_image' => $save_url,
            ]);
            //toaster
            $notification = array(
            'message' => 'About Page Updated with Image Successfully',
            'alert-type' => 'info'
            );
            return redirect()->back()->with($notification);
        } else {
            About::findOrFail($about_id)->update([
                'title' => $request->title,
                'short_title' => $request->short_title,
                'short_description' => $request->short_description,
                'long_description' => $request->long_description,
            ]);
            //toaster 
            $notification = array(
            'message' => 'About Page Updated without Image Successfully',
            'alert-type' => 'info'
            );
            return redirect()->back()->with($notification);
        }
    } //end method

    public function HomeAbout() {
        $aboutPage = About::find(1);
        return view('frontend.about_page',compact('aboutPage'));
    } //end method

    public function MultiImage() {
        return view('admin.about_page.multi_image');
    } //end method

    public function StoreMultiImage(Request $request) {
        $image = $request->file('multi_image');
        foreach($image as $multi_image) {
            $name_gen = hexdec(uniqid()).'.'.$multi_image->getClientOriginalExtension(); //id.jpg(dinh dang ten anh)
            Image::make($multi_image)->resize(220,220)->save('upload/multi/'.$name_gen);
            $save_url = 'upload/multi/'.$name_gen; //save db

            MultiImage::insert([
                'multi_image' => $save_url,
                'created_at' => Carbon::now()
            ]);
        } //end of foreach
        //toaster
            $notification = array(
            'message' => 'Multi Images inserted Successfully',
            'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
    } //end method

    public function AllMultiImage() {
        $allMultiImage = MultiImage::all();
        return view('admin.about_page.all_multi_image', compact('allMultiImage'));
    }// end method

    public function EditMultiImage($id) {
        $multiImage = MultiImage::findOrFail($id);
        return view('admin.about_page.edit_multi_image', compact('multiImage'));
    }// end method

    public function UpdateMultiImage(Request $request) {
        $multi_image_id = $request->id;
        if($request->file('multi_image')) {
            $image = $request->file('multi_image'); 
            $name_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension(); //id.jpg(dinh dang ten anh)
            Image::make($image)->resize(523,605)->save('upload/multi/'.$name_gen);
            $save_url = 'upload/multi/'.$name_gen; //save db

            MultiImage::findOrFail($multi_image_id)->update([
                'multi_image' => $save_url,
            ]);
            //toaster
            $notification = array(
            'message' => 'Updated Multi Image Successfully',
            'alert-type' => 'info'
            );
            return redirect()->route('all.multi.image')->with($notification);
        }
    }//end method

    public function DeleteMultiImage($id){
        $multi_image = MultiImage::findOrFail($id);
        $image = $multi_image->multi_image;
        unlink($image);

        MultiImage::findOrFail($id)->delete();

        $notification = array(
            'message' => 'Multi Image Deleted Successfully',
            'alert-type' => 'info'
            );
            return redirect()->back()->with($notification); 
    }//end method
}