<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\BlogCategory;
use Image;
use Illuminate\Support\Carbon;

class BlogController extends Controller
{
    public function AllBlog(){
        $blogs = Blog::latest()->get();
        return view('admin.blogs.blogs_all', compact('blogs'));
    }//end method   

    public function AddBlog(){
        $categories = BlogCategory::orderBy('blog_category','ASC')->get();
        return view('admin.blogs.blogs_add', compact('categories'));
    }//end method

    public function StoreBlog(Request $request) {
        $image = $request->file('blog_image');
        $name_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(430,327)->save('upload/blog/'.$name_gen);
        $save_url = 'upload/blog/'.$name_gen;

        Blog::insert([
            'blog_category_id' => $request->blog_category_id,
            'blog_title' => $request->blog_title,
            'blog_tags' => $request->blog_tags,
            'blog_description' => $request->blog_description,
            'blog_image' => $save_url,
            'created_at' => Carbon::now()
        ]);
        $notification = array(
            'message' => 'Blog Data update Successfully',
            'alert-type' => 'info'
        );
        return redirect()->route('all.blog')->with($notification);
    }//end method

    public function EditBlog($id) {
       $blogs = Blog::findOrFail($id);
       $categories = BlogCategory::orderBy('blog_category','ASC')->get();
       return view('admin.blogs.blogs_edit', compact('blogs', 'categories'));
    }//end method
 
    public function UpdateBlog(Request $request) {
        $blog_id = $request->id;
        if($request->file('blog_image')) {
            $image = $request->file('blog_image'); 
            $name_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension(); //id.jpg(dinh dang ten anh)
            Image::make($image)->resize(430,327)->save('upload/blog/'.$name_gen);
            $save_url = 'upload/blog/'.$name_gen; //save db

            Blog::findOrFail($blog_id)->update([
                'blog_category_id' => $request->blog_category_id,
                'blog_title' => $request->blog_title,
                'blog_tags' => $request->blog_tags,
                'blog_description' => $request->blog_description,
                'blog_image' => $save_url,
            ]);
            $notification = array(
                'message' => 'Blog update with Image Successfully',
                'alert-type' => 'info'
            );
            return redirect()->route('all.blog')->with($notification);
        } else {
            Blog::findOrFail($blog_id)->update([
                'blog_category_id' => $request->blog_category_id,
                'blog_title' => $request->blog_title,
                'blog_tags' => $request->blog_tags,
                'blog_description' => $request->blog_description,
            ]);
            $notification = array(
                'message' => 'Portfolio update without Image Successfully',
                'alert-type' => 'info' 
            );
            return redirect()->route('all.blog')->with($notification);
        }
    }//end method

    public function DeleteBlog($id) {
        $blog = Blog::findOrFail($id);
        $image = $blog->blog_image;
        unlink($image);

        Blog::findOrFail($id)->delete();
        $notification = array(
            'message' => 'Blog Deleted Successfully',
            'alert-type' => 'info' 
        );
        return redirect()->back()->with($notification);
    }//end method

    public function BlogDetails($id) {
        $blog = Blog::findOrFail($id);
        $allBlog = Blog::latest()->limit(5)->get();
        $categories = BlogCategory::orderBy('blog_category','ASC')->get();
        return view('frontend.blog_details', compact('blog','allBlog','categories'));
    }//end method

    public function CategoryBlog($id) {
        $category = BlogCategory::findOrFail($id);
        $blogPost = Blog::where('blog_category_id',$id)->orderBy('id','DESC')->get();
        $allBlog = Blog::latest()->limit(5)->get();
        $categories = BlogCategory::orderBy('blog_category','ASC')->get();
        return view('frontend.category_blog_details', compact('blogPost','allBlog','categories','category'));
    }//end method

    public function HomeBlog() {
        $allBlog = Blog::latest()->get();
        $categories = BlogCategory::orderBy('blog_category','ASC')->get();
        return view('frontend.blog', compact('allBlog','categories'));
    }//end method
}
