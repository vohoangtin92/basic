<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        //toaster
        $notification = array(
            'message' => 'User Logout Successfully',
            'alert-type' => 'info'
        );

        return redirect('/login')->with($notification);
    } //end method

    public function Profile() {
        $id = Auth::user()->id; //get userId from Authentication User
        $adminData = User::find($id); //find User with ID that I want to find it, compare ID to get information
        return view('admin.admin_profile_view', compact('adminData')); //pass admindata into page.
    } //end method

    public function EditProfile() {
        $id = Auth::user()->id; //get userId from Authentication User
        $editData = User::find($id); //find User with ID that I want to find it, compare ID to get information
        return view('admin.admin_profile_edit', compact('editData')); //pass admindata into page.
    } //end method

    public function StoreProfile(Request $request) {
        $id = Auth::user()->id; //get userId from Authentication User
        $data = User::find($id); //find User with ID that I want to find it, compare ID to get information
        $data->name = $request->name;
        $data->username = $request->username;
        $data->email = $request->email;

        if($request->file('profile_image')){
            $file = $request->file('profile_image');

            $filename = date('YmdHi').$file->getClientOriginalName(); //dinh đang dat ten cho image
            $file->move(public_path('upload/admin_images'), $filename);
            $data['profile_image'] = $filename;
        }
        $data->save();

        //toaster
        $notification = array(
            'message' => 'Admin Profile Updated Successfully',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.profile')->with($notification);
    } //end method

    public function ChangePassword() {
        return view('admin.admin_change_password');
    } //end method

    public function UpdatePassword(Request $request) {
        $validateData = $request->validate([
            'oldpassword' => 'required',
            'newpassword' => 'required',
            'confirm_password' => 'required|same:newpassword',  
        ]);

        $hashedPassword = Auth::user()->password;
        if(Hash::check($request->oldpassword, $hashedPassword)){
            $users = User::find(Auth::id()); 
            $users->password = bcrypt($request->newpassword); 
            $users->save();

            session()->flash('message','Password Updated Successfully');
            return redirect()->back();
        } else {
            session()->flash('message','Old Password is not match');
            return redirect()->back();
        }
    } //end method

}
